README
=====================

Этот README документирует описывает все шаги, необходимые для создания и запуска веб-приложения.

### Настройки Docker

##### Установка

* [Подробное руководство по установке](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

##### Команды для запуска docker без sudo

* `sudo groupadd docker`
* `sudo gpasswd -a ${USER} docker`
* `newgrp docker`
* `sudo service docker restart`

##### Проверка работоспособности docker без sudo

* `docker run hello-world`

### Настройки Docker-compose

##### Установка

* [Подробное руководство по установке](https://docs.docker.com/compose/install/)

##### Команда для запуска docker-compose без sudo

* `sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose`

##### Применение изменений

* `reboot`

### Fabric

Файл `fabfile.py` содержит ряд функций, которые помогают при локальной разработке.

##### Установка

* `sudo apt-get install fabric`

##### Основные команды

* `fab dev` - запустить локально веб приложение
* `fab makemigrations` - создать файл миграций
* `fab migrate` - применить миграции
* `fab load_backup` - загрузить резервную копию базы
* `fab createsuperuser` - создать супер пользователя

### Локальная разработка

##### Команды

* `docker-compose build` - создать контейнеры docker
* `fab dev` - зупустить веб приложение
* `fab load_backup` - загрузить резервную копию базы (если имеется)
* `fab migrate` - применить миграции

##### Доступ

* http://localhost:8000
 
### Развертывание веб приложения на сервере

##### Команды

* `cd server`
* `chmod +x run.sh`
* `./run.sh`
