from fabric.api import local
import os


def load_backup():
    current_path = os.path.dirname(__file__)
    list_filename = [file for file in os.listdir(current_path) if file.endswith('.dump') or file.endswith('.sql')]

    if not list_filename:
        return
    else:
        backup_path = '{}/{}'.format(current_path, list_filename[0])

    if '.dump' in backup_path:
        binary = 'True'
    elif '.sql' in backup_path:
        binary = 'False'
    else:
        return

    if binary == 'True':
        cmd = "docker exec -i $(docker ps | grep db_ | awk '{{ print $1 }}') pg_restore --no-acl --no-owner -U postgres -d postgres < {0}".format(backup_path)
    else:
        cmd = "docker exec -i $(docker ps | grep db_ | awk '{{ print $1 }}') psql -U postgres -d postgres < {0}".format(backup_path)

    local(cmd)


def makemigrations(app=''):
    local("docker exec -i $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py makemigrations {}".format(app))


def migrate(app=''):
    local("docker exec -i $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py migrate {}".format(app))


def createsuperuser():
    local("docker exec -it $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py createsuperuser")


def bash():
    local("docker exec -it $(docker ps | grep server_ | awk '{{ print $1 }}') bash")


def shell():
    local("docker exec -it $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py shell")


def dev():
    local("docker-compose run --rm --service-ports server")


def kill():
    local("docker kill $(docker ps -q)")
