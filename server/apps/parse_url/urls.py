from django.urls import path

from apps.parse_url.views import IndexView


urlpatterns = [
    path('', IndexView.as_view(), name='index'),
]
