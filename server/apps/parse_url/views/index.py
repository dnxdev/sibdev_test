from django.views import View
from django.shortcuts import render

from apps.parse_url.models import ParsePage


class IndexView(View):
    def get(self, request, *args, **kwargs):
        data = {
            'result_parse': ParsePage.objects.filter(is_parse=True).only(
                'work_report', 'result'
            ),
        }

        return render(request, 'index.html', data)

