from datetime import datetime

import requests
from lxml import html, etree
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from config.celery import app
from apps.parse_url.models import ParsePage
from apps.parse_url.consumers import ROOM_NAME


channel_layer = get_channel_layer()


@app.task
def parse_page_task(url):
    date = datetime.now().strftime("%d.%m.%Y %H:%M:%S")
    data = {}

    try:
        response = requests.get(url)
    except requests.exceptions.RequestException as error:
        data.update({
            'work_report': '{}: {}'.format(date, error),
            'result': url,
        })

    if not bool(data) and response.status_code != 200:
        data.update({
            'work_report': '{}: status code - {}'.format(date, response.status_code),
            'result': url,
        })

    if not bool(data):
        tree = html.fromstring(
            response.text,
            parser=etree.HTMLParser(encoding="utf-8")
        )

        title = tree.xpath('.//title')
        title =title[0].text.strip() if title else ''

        headers = ' --- '.join(map(
            lambda v: v.text.strip(),
            filter(
                lambda v: v.text is not None,
                tree.xpath('.//h1'),
            )
        ))

        data.update({
            'work_report': '{}: success'.format(date),
            'result': '{} - title: {}, encoding: {}, H1: {}'.format(
                url,
                title if title else 'UNDEFINED',
                response.encoding,
                headers if headers else 'UNDEFINED',
            )
        })

    ParsePage.objects.filter(
        url=url
    ).update(
        is_parse=True,
        **data
    )

    async_to_sync(channel_layer.group_send)(
        ROOM_NAME,
        {
            'type': 'chat_message',
            'message': {
                'message': data,
            }
        },
    )
