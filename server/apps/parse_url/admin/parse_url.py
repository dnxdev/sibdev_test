from django.contrib import admin


class ParsePageAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('url',)
        }),
        ('Сдвиг по времени', {
            'fields': ('minutes', 'seconds'),
        }),
        ('Результат работы парсера', {
            'fields': ('is_parse', 'work_report', 'result'),
        }),
    )

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['is_parse', 'work_report', 'result']

        if obj:
            readonly_fields += ['url', 'minutes', 'seconds']

        return readonly_fields
