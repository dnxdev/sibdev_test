from django.contrib import admin

from .parse_url import ParsePageAdmin
from apps.parse_url.models import ParsePage


admin.site.register(ParsePage, ParsePageAdmin)
