import json

from channels.generic.websocket import AsyncWebsocketConsumer


ROOM_NAME = 'result_parser'


class ResultParserConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add(
            ROOM_NAME,
            self.channel_name,
        )

        await self.accept()

    async def disconnect(self, code):
        """
        Called when the WebSocket closes for any reason.
        """
        # Leave the room
        await self.channel_layer.group_discard(
            ROOM_NAME,
            self.channel_name,
        )
        await self.close()

    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))
