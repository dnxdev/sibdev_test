from django.db.models.signals import post_save

from .start_parse import start_parse
from apps.parse_url.models import ParsePage


def connect():
    post_save.connect(start_parse, sender=ParsePage)
