from apps.parse_url.tasks import parse_page_task


def start_parse(sender, instance, created, **kwargs):
    if not instance.is_parse:
        parse_page_task.apply_async(
            args=[instance.url],
            countdown=60 * instance.minutes + instance.seconds,
        )
