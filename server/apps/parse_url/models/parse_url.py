from django.db import models


class ParsePage(models.Model):
    url = models.URLField(
        verbose_name='URL',
        unique=True,
    )
    # Сдвиг по времени
    minutes = models.PositiveIntegerField(
        verbose_name='Минуты',
        default=0,
    )
    seconds = models.PositiveIntegerField(
        verbose_name='Секунды',
        default=0,
    )
    is_parse = models.BooleanField(
        verbose_name='Данные парсились',
        default=False,
    )
    work_report = models.TextField(
        verbose_name='Отчет о работе',
        blank=True,
        null=True,
    )
    result = models.TextField(
        verbose_name='Результат работы парсера',
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.url

    class Meta:
        verbose_name = 'Адрес страницы'
        verbose_name_plural = 'Адреса страниц'
