from django.db.models import Sum

from apps.test_3.models import Table1, Table4


def write_table_4():
    Table4.objects.bulk_create(list(map(
        lambda obj: Table4(
            internal_number=obj.table_3.internal_number,
            position=obj.table_3.position,
            name_surname='{} {}'.format(obj.name, obj.surname),
            salary_month=float(obj.salary_year) / 12,
            tax=obj.tax,
            month=obj.table_2.last().month
        ),
        Table1.objects.all().annotate(tax=Sum('table_2__taxes')).prefetch_related('table_2', 'table_3')
    )))
