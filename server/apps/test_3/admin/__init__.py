from django.contrib import admin

from apps.main.admin.base_admin import BaseAdmin
from apps.test_3.models import Table1, Table2, Table3, Table4


admin.site.register(Table1, BaseAdmin)
admin.site.register(Table2, BaseAdmin)
admin.site.register(Table3, BaseAdmin)
admin.site.register(Table4, BaseAdmin)
