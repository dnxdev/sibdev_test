from django.db import models


class Table4(models.Model):
    internal_number = models.PositiveIntegerField()
    name_surname = models.CharField(
        max_length=255,
    )
    position = models.CharField(
        max_length=255,
    )
    salary_month = models.FloatField()
    tax = models.PositiveIntegerField()
    month = models.DateField()
