from django.db import models


class Table2(models.Model):
    table_1 = models.ForeignKey(
        'test_3.Table1',
        on_delete=models.CASCADE,
        related_name='table_2',
    )
    month = models.DateField()
    taxes = models.PositiveIntegerField()
