from django.db import models


class Table3(models.Model):
    table_1 = models.OneToOneField(
        'test_3.Table1',
        on_delete=models.CASCADE,
        related_name='table_3'
    )
    internal_number = models.PositiveIntegerField()
    position = models.CharField(
        max_length=255,
    )
