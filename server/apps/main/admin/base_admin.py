from django.contrib import admin


class BaseAdmin(admin.ModelAdmin):
    def __init__(self, model, admin_site):
        self.list_display = [field.name for field in model._meta.fields if field.name != "id"]
        super(BaseAdmin, self).__init__(model, admin_site)
