#!/bin/bash
apt update
apt install -y nginx python3-pip curl libssl-dev libcurl4-openssl-dev redis-server supervisor

# setting nginx
rm -v /etc/nginx/nginx.conf
cp nginx.conf /etc/nginx/
nginx -s reload

# setting redis
rm -v /etc/redis/redis.conf
cp redis.conf /etc/redis/
systemctl restart redis.service

# install python dependencies
pip3 install -r requirements.txt

# settings django
python3 manage.py collectstatic --noinput
python3 manage.py migrate

# start web application
cp web.conf /etc/supervisor/conf.d/
supervisorctl reread
supervisorctl update