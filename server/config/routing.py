from django.urls import path
# from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter

from apps.parse_url.consumers import ResultParserConsumer


application = ProtocolTypeRouter({
    "websocket": URLRouter([
        path('ws/connect/', ResultParserConsumer),
    ]),
})
