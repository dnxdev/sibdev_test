DASHBOARD = [
    {
        'name': 'Настройка парсера',
        'app_label': 'Настройка парсера',
        'app_url': '/admin/',
        'has_module_perms': True,
        'models': [
            {
                'name': 'URL  страниц',
                'object_name': 'ParsePage',
                'perms': {
                    'add': True,
                    'change': True,
                    'delete': True,
                },
                'admin_url': '/admin/parse_url/parsepage/',
                'add_url': '/admin/parse_url/parsepage/add/',
            },
        ]
    },
    {
        'name': 'Задание 3',
        'app_label': 'Задание 3',
        'app_url': '/admin/',
        'has_module_perms': True,
        'models': [
            {
                'name': 'Table 1',
                'object_name': 'Table1',
                'perms': {
                    'add': True,
                    'change': True,
                    'delete': True,
                },
                'admin_url': '/admin/test_3/table1/',
                'add_url': '/admin/test_3/table1/add/',
            },
            {
                'name': 'Table 2',
                'object_name': 'Table2',
                'perms': {
                    'add': True,
                    'change': True,
                    'delete': True,
                },
                'admin_url': '/admin/test_3/table2/',
                'add_url': '/admin/test_3/table2/add/',
            },
            {
                'name': 'Table 3',
                'object_name': 'Table3',
                'perms': {
                    'add': True,
                    'change': True,
                    'delete': True,
                },
                'admin_url': '/admin/test_3/table3/',
                'add_url': '/admin/test_3/table3/add/',
            },
            {
                'name': 'Table 4',
                'object_name': 'Table4',
                'perms': {
                    'add': True,
                    'change': True,
                    'delete': True,
                },
                'admin_url': '/admin/test_3/table4/',
                'add_url': '/admin/test_3/table4/add/',
            },
        ]
    },
    {
        'name': 'Пользователи',
        'app_label': 'Пользователи',
        'app_url': '/admin/',
        'has_module_perms': True,
        'models': [
            {
                'name': 'Пользователи',
                'object_name': 'MyUser',
                'perms': {
                    'add': True,
                    'change': True,
                    'delete': True,
                },
                'admin_url': '/admin/account/myuser/',
                'add_url': '/admin/account/myuser/add/'
            },
            {
                'name': 'Группы пользователей',
                'object_name': 'Group',
                'perms': {
                    'add': True,
                    'change': True,
                    'delete': True,
                },
                'admin_url': '/admin/auth/group/',
                'add_url': '/admin/auth/group/add/'
            },
        ]
    },
]
