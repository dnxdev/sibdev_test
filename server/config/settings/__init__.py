from .installed_apps import *
from .settings import *
from .locale import *
from .dashboard import *
from .celery import *
