from datetime import datetime

from django.db import migrations


def add_data(apps, schema_editor):
    Table1 = apps.get_model("test_3", "Table1")
    Table2 = apps.get_model("test_3", "Table2")
    Table3 = apps.get_model("test_3", "Table3")

    table_1_1 = Table1.objects.create(name='John', surname='Terrible', salary_year=11000)
    table_1_2 = Table1.objects.create(name='Maggie', surname='Woodstock', salary_year=15000)
    table_1_3 = Table1.objects.create(name='Joel', surname='Muegos', salary_year=22000)
    table_1_4 = Table1.objects.create(name='Jeroen', surname='van Kapf', salary_year=44000)

    Table2.objects.bulk_create([
        Table2(month=datetime(15, 1, 1), taxes=250, table_1=table_1_1),
        Table2(month=datetime(15, 2, 1), taxes=267, table_1=table_1_1),
        Table2(month=datetime(15, 1, 1), taxes=300, table_1=table_1_2),
        Table2(month=datetime(15, 2, 1), taxes=350, table_1=table_1_2),
        Table2(month=datetime(15, 1, 1), taxes=245, table_1=table_1_3),
        Table2(month=datetime(15, 2, 1), taxes=356, table_1=table_1_3),
        Table2(month=datetime(15, 1, 1), taxes=246, table_1=table_1_4),
        Table2(month=datetime(15, 2, 1), taxes=356, table_1=table_1_4),
        Table2(month=datetime(15, 3, 1), taxes=412, table_1=table_1_3),
    ])

    Table3.objects.bulk_create([
        Table3(internal_number=32894, position='Manager', table_1=table_1_1),
        Table3(internal_number=23409, position='Top Manager', table_1=table_1_2),
        Table3(internal_number=23908, position='CEO', table_1=table_1_3),
        Table3(internal_number=128, position='Board Chairman', table_1=table_1_4),
    ])


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('test_3', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_data),
    ]
