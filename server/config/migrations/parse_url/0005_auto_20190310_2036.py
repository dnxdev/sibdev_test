# Generated by Django 2.0.6 on 2019-03-10 13:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('parse_url', '0004_auto_20190310_2022'),
    ]

    operations = [
        migrations.AlterField(
            model_name='parsepage',
            name='url',
            field=models.URLField(unique=True, verbose_name='URL'),
        ),
    ]
